package com.example.practica01kotlin

class Operaciones (var numA:Float, var numB:Float){
    public fun sumar():Float{
        return this.numA + numB
    }

    public fun resta():Float{
        return this.numA - numB
    }

    public fun multi():Float{
        return this.numA * numB
    }

    public fun division():Float{
        return this.numA / numB
    }
}